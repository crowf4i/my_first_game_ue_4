# My First Game

## Hello! This project is my first game on Unreal Engine 4, and I used Blueprint exclusively to create it. Here's what I've learned and what deserves attention:

### Learned:
- Blueprint: The primary tool for creating game logic without writing code.
- Basic game logic: Creating fundamental game mechanics such as character control, item collection, etc.
- Events: Responding to various events in the game, such as collisions or button presses.
- Widgets and interface: Creating a user interface for the game, including menus, text fields, etc.
- Working with materials: Learning the basics of creating and configuring materials for objects and surfaces.
- Audio principles: Implementing sound effects and music in the game.
- Animation control: Using animations for characters and objects to bring them to life and add realism.
- Component and system usage: Studying how to work with components and systems in Unreal Engine to create complex interactions in the game.

### Areas for improvement:
- Materials: Needing a deeper understanding of creating and configuring materials to achieve higher-quality visual effects.
- Audio: Developing higher-quality sound effects and improving music handling to create a more atmospheric audio environment.
- Object creation: Learning the editor tools for creating custom objects and their further use in the game.
- Diverse implementation: Exploring various methods of implementing game mechanics on different objects and improving the quality of their execution.

## This project has been a fantastic learning experience for me, and I intend to enhance my skills to create even more captivating games in the future!
